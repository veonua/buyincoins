package ua.veon.buyincoins.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/26/13
 * Time: 3:46 PM
 */
public class OrderContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.buyincoins.orders";
    // Used for the UriMacher
    private static final int ORDERS = 10;
    private static final int ORDER_ID = 20;

    private static final String BASE_PATH = "item";
    public static final Uri ORDER_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    public static final String CONTENT_TYPE_ORDERS = ContentResolver.CURSOR_DIR_BASE_TYPE + "/orders";
    public static final String CONTENT_ORDER_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/order";

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, ORDERS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", ORDER_ID);
    }

    private Helper helper;

    public OrderContentProvider() {
        super();
    }

    @Override
    public boolean onCreate() {
        helper = new Helper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteDatabase readable = helper.getReadableDatabase();

        if (TextUtils.isEmpty(sortOrder))
            sortOrder = Helper.COLUMN_DATE + " desc";

        if (TextUtils.isEmpty(selection))
            selection = Helper.COLUMN_HIDDEN + " = 0";

        Cursor ret = readable.query(Helper.TABLE_ORDERS, projection, selection, selectionArgs, null, null, sortOrder, null);
        ret.setNotificationUri(getContext().getContentResolver(), uri);
        return ret;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ORDERS:
                return CONTENT_TYPE_ORDERS;
            case ORDER_ID:
                return CONTENT_ORDER_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Uri ret = upsertOrder(db, contentValues);
            getContext().getContentResolver().notifyChange(ret, null, false);
            return ret;
        } finally {
            db.close();
        }
    }

    private Uri upsertOrder(SQLiteDatabase writableDb, ContentValues values) {
        String id = values.getAsString(BaseColumns._ID);

        if (writableDb.update(Helper.TABLE_ORDERS, values, BaseColumns._ID + "= ?", new String[]{id}) < 1)
            writableDb.insert(Helper.TABLE_ORDERS, null, values);

        return ORDER_URI.buildUpon().appendEncodedPath(id).build();
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int len = values.length;
        if (len < 1) return 0;

        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            for (ContentValues val : values) {
                upsertOrder(db, val);
            }
        } finally {
            db.close();
        }
        getContext().getContentResolver().notifyChange(uri, null, false);
        return len;
    }
}