package ua.veon.buyincoins.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/26/13
 * Time: 3:46 PM
 */
@SuppressWarnings("UnusedDeclaration")
public class MessageContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.buyincoins.messages";
    // Used for the UriMacher
    private static final int MESSAGES = 10;
    private static final int MESSAGE_ID = 11;
    private static final int THREADS = 30;
    private static final int THREAD_ID = 31;

    private static final String MESSAGE_PATH = "messages";
    private static final String THREAD_PATH = "threads";
    public static final Uri MESSAGE_URI = Uri.parse("content://" + AUTHORITY + "/" + MESSAGE_PATH);
    public static final Uri THREAD_URI = Uri.parse("content://" + AUTHORITY + "/" + THREAD_PATH);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, MESSAGE_PATH, MESSAGES);
        uriMatcher.addURI(AUTHORITY, MESSAGE_PATH + "/#", MESSAGE_ID);
        uriMatcher.addURI(AUTHORITY, THREAD_PATH, THREADS);
        uriMatcher.addURI(AUTHORITY, THREAD_PATH + "/#", THREAD_ID);
    }

    private Helper helper;

    public MessageContentProvider() {
        super();
    }

    @Override
    public boolean onCreate() {
        helper = new Helper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        int tp = uriMatcher.match(uri);
        int tpmod = tp % 10;

        switch (tp) {
            case THREADS: {
                queryBuilder.setTables(Helper.TABLE_MSG_THREADS);
                if (TextUtils.isEmpty(sortOrder))
                    sortOrder = Helper.COLUMN_CHANGE_DATE + " desc";
                break;
            }

            case THREAD_ID: {
                queryBuilder.setTables(Helper.TABLE_MESSAGES);
                queryBuilder.appendWhere(Helper.COLUMN_THREAD + "=" + uri.getLastPathSegment());

                if (TextUtils.isEmpty(sortOrder))
                    sortOrder = Helper.COLUMN_DATE + " asc";
                break;
            }

            default:
                throw new UnsupportedOperationException();
        }

        Cursor ret = queryBuilder.query(helper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sortOrder);
        ret.setNotificationUri(getContext().getContentResolver(), uri);
        return ret;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case MESSAGES:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/messages";
            case MESSAGE_ID:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/message";
            case THREADS:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/msg_threads";
            case THREAD_ID:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/msg_thread";
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    private Uri upsertThreads(SQLiteDatabase writableDb, ContentValues values) {
        String id = values.getAsString(BaseColumns._ID);

        if (writableDb.update(Helper.TABLE_MSG_THREADS, values, BaseColumns._ID + "= ?", new String[]{id}) < 1)
            writableDb.insert(Helper.TABLE_MSG_THREADS, null, values);

        return THREAD_URI.buildUpon().appendEncodedPath(id).build();
    }

    private Uri insertMessage(SQLiteDatabase writableDb, long threadId, ContentValues values) {
        values.put(Helper.COLUMN_THREAD, threadId);
        long id = writableDb.insert(Helper.TABLE_MESSAGES, null, values);

        return THREAD_URI.buildUpon().appendEncodedPath(String.valueOf(id)).build();
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int len = values.length;
        if (len < 1) return 0;

        SQLiteDatabase db = helper.getWritableDatabase();

        try {
            switch (uriMatcher.match(uri)) {
                case THREADS: {
                    for (ContentValues val : values) {
                        upsertThreads(db, val);
                    }
                    break;
                }

                case THREAD_ID: {
                    long id = Long.parseLong(uri.getLastPathSegment());
                    int count = 0;
                    Cursor c = db.rawQuery("select count(*) from " + Helper.TABLE_MESSAGES + " where " + Helper.COLUMN_THREAD + " = " + id, null);
                    if (c.moveToFirst())
                        count = c.getInt(0);
                    c.close();

                    if (count == values.length) return 0;

                    for (int i = count; i < values.length; i++) {
                        insertMessage(db, id, values[i]);
                    }
                    break;
                }
            }
        } finally {
            db.close();
        }

        getContext().getContentResolver().notifyChange(uri, null, false);
        return len;
    }
}