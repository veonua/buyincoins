package ua.veon.buyincoins.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.v4.content.CursorLoader;
import ua.veon.buyincoins.controllers.SessionParser;
import ua.veon.buyincoins.controllers.StatusHelper;
import ua.veon.buyincoins.models.*;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/26/13
 * Time: 7:41 PM
 */
public class DbManager {

    public static void upsertOrders(final Context context, final String user, final Iterable<OrderHistoryRecord> records) {
        ArrayList<ContentValues> res = new ArrayList<ContentValues>();
        for (OrderHistoryRecord record : records) {
            ContentValues values = new ContentValues(4);
            values.put(BaseColumns._ID, record.id);
            values.put(Helper.COLUMN_OWNER, user);
            values.put(Helper.COLUMN_DATE, record.date.getTime());
            String s = record.status;
            values.put(Helper.COLUMN_STATUS, StatusHelper.parseSite(s).ordinal());
            values.put(Helper.COLUMN_STATUS_TEXT, s);

            values.put(Helper.COLUMN_PRICE, record.price);
            values.put(Helper.COLUMN_UNITS, record.productCount);
            res.add(values);
        }

        context.getContentResolver().bulkInsert(OrderContentProvider.ORDER_URI, res.toArray(new ContentValues[res.size()]));
    }


    public static void upsertMessageThreads(final Context context, final String user, final Iterable<MessageThreadRecord> records) {
        ArrayList<ContentValues> res = new ArrayList<ContentValues>();
        for (MessageThreadRecord record : records) {
            ContentValues values = new ContentValues(7);

            values.put(BaseColumns._ID, record.id);
            values.put(Helper.COLUMN_OWNER, user);
            values.put(Helper.COLUMN_DATE, record.create_time.getTime());
            values.put(Helper.COLUMN_CHANGE_DATE, record.replied_time.getTime());
            values.put(Helper.COLUMN_TEXT, record.subject);
            values.put(Helper.COLUMN_LAST_REPLIED_BY, record.last_replied_by);
            values.put(Helper.COLUMN_IS_NEW, record.is_new);
            values.put(Helper.COLUMN_IS_NEW_CS, record.is_new_for_cs);

            res.add(values);
        }

        context.getContentResolver().bulkInsert(MessageContentProvider.THREAD_URI, res.toArray(new ContentValues[res.size()]));
    }

    public static void upsertOrder(final Context context, final String user, final OrderLocalModel model) {

        ContentValues values = new ContentValues(5);
        values.put(BaseColumns._ID, model.order_id);

        values.put(Helper.COLUMN_OWNER, user);
        if (model.isCommentChanged())
            values.put(Helper.COLUMN_COMMENT, model.getComment());
        if (model.isDateChanged())
            values.put(Helper.COLUMN_DATE, model.getDate().getTime());
        if (model.isStatusChanged()) {
            values.put(Helper.COLUMN_STATUS, model.getStatus().ordinal());
            values.put(Helper.COLUMN_STATUS_TEXT, model.getStatusString());
        }

        context.getContentResolver().insert(OrderContentProvider.ORDER_URI, values);
    }

    public static final String[] list_projection = new String[]{BaseColumns._ID, Helper.COLUMN_OWNER, Helper.COLUMN_DATE, Helper.COLUMN_STATUS};

    public static CursorLoader getOrdersLoader(final Context context) {
        return new CursorLoader(context, OrderContentProvider.ORDER_URI, list_projection, null, null, null);
    }

    public static final String[] threads_projection = new String[]{BaseColumns._ID, Helper.COLUMN_TEXT, Helper.COLUMN_CHANGE_DATE, Helper.COLUMN_LAST_REPLIED_BY, Helper.COLUMN_IS_NEW, Helper.COLUMN_IS_NEW_CS};

    public static CursorLoader getMessagesLoader(final Context context) {
        return new CursorLoader(context, MessageContentProvider.THREAD_URI, threads_projection, null, null, null);
    }

    public static OrderLocalModel getOrderById(final Context context, String orderId) throws IndexOutOfBoundsException {
        final String[] model_projection = new String[]{BaseColumns._ID, Helper.COLUMN_DATE, Helper.COLUMN_COMMENT, Helper.COLUMN_STATUS, Helper.COLUMN_STATUS_TEXT};

        Cursor c = context.getContentResolver().query(OrderContentProvider.ORDER_URI, model_projection, BaseColumns._ID + " = ?", new String[]{orderId}, null);
        try {
            if (c.moveToFirst()) {
                OrderLocalModel m = new OrderLocalModel(c.getString(0));
                m.setDate(new Date(c.getLong(1)));
                m.setComment(c.getString(2));
                m.setStatus(Status.values()[c.getInt(3)], c.getString(4));
                m.flagSaved();
                return m;
            } else
                throw new IndexOutOfBoundsException("no order found");
        } finally {
            c.close();
        }
    }

    public static final String[] messages_projection = new String[]{BaseColumns._ID, Helper.COLUMN_TEXT, Helper.COLUMN_DATE, Helper.COLUMN_AUTHOR, Helper.COLUMN_AUTHOR_TYPE};

    public static CursorLoader getThread(Context context, long id) {
        Uri uri = MessageContentProvider.THREAD_URI.buildUpon().appendPath(String.valueOf(id)).build();
        return new CursorLoader(context, uri, messages_projection, null, null, null);
    }

    public static void upsertMessages(Context context, SessionParser.ParseThreadResult data) {
        ArrayList<ContentValues> res = new ArrayList<ContentValues>();
        for (MessageRecord record : data.list) {
            ContentValues values = new ContentValues(7);

            values.put(Helper.COLUMN_DATE, record.create_time.getTime());
            values.put(Helper.COLUMN_TEXT, record.text);
            values.put(Helper.COLUMN_AUTHOR, record.author);
            values.put(Helper.COLUMN_AUTHOR_TYPE, record.author_type);

            res.add(values);
        }

        context.getContentResolver().bulkInsert(MessageContentProvider.THREAD_URI.buildUpon().appendPath(String.valueOf(data.threadId)).build(), res.toArray(new ContentValues[res.size()]));
    }
}
