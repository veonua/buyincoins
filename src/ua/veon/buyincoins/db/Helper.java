package ua.veon.buyincoins.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/26/13
 * Time: 7:54 PM
 */
class Helper extends SQLiteOpenHelper {
    private static final String TAG = "Helper";

    public static final String TABLE_ORDERS = "orders";
    public static final String COLUMN_OWNER = "owner";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_CHANGE_DATE = "change_date";
    public static final String COLUMN_HIDDEN = "hidden";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_UNITS = "units";
    public static final String COLUMN_STATUS_TEXT = "_status";
    public static final String COLUMN_COMMENT = "comment";

    public static final String COLUMN_AUTHOR = "_by";
    public static final String COLUMN_LAST_REPLIED_BY = COLUMN_AUTHOR;
    public static final String COLUMN_THREAD = "thread";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_AUTHOR_TYPE = "_type";

    public static final String COLUMN_IS_NEW = "_new";
    public static final String COLUMN_IS_NEW_CS = "_new_cs";

    public static final String TABLE_MESSAGES = "messages";
    public static final String TABLE_MSG_THREADS = "msg_threads";

    private static final String DATABASE_NAME = "buyincoins.db";
    private static final int DATABASE_VERSION = 2;

    private static final String ORDERS_CREATE = "create table "
            + TABLE_ORDERS + "("
            + BaseColumns._ID + " VARCHAR(20) primary key not null"
            + ", " + COLUMN_OWNER + " text not null"
            + ", " + COLUMN_DATE + " date not null"
            + ", " + COLUMN_HIDDEN + " bool default 0"
            + ", " + COLUMN_COMMENT + " text"
            + ", " + COLUMN_STATUS + " int default 0"
            + ", " + COLUMN_STATUS_TEXT + " text"
            + ", " + COLUMN_PRICE + " decimal(16,2) default 0"
            + ", " + COLUMN_UNITS + " int default 0"
            + ");";

    private static final String MESSAGES_CREATE = "create table "
            + TABLE_MESSAGES + "("
            + BaseColumns._ID + " integer primary key autoincrement"
            + ", " + COLUMN_THREAD + " int not null"
            + ", " + COLUMN_AUTHOR + " text"
            + ", " + COLUMN_AUTHOR_TYPE + " int not null"
            + ", " + COLUMN_DATE + " date not null"
            + ", " + COLUMN_TEXT + " text"
            + ");";

    private static final String MSG_THREADS_CREATE = "create table "
            + TABLE_MSG_THREADS + "("
            + BaseColumns._ID + " int primary key not null"
            + ", " + COLUMN_OWNER + " text not null"
            + ", " + COLUMN_DATE + " date not null"
            + ", " + COLUMN_CHANGE_DATE + " date not null"
            + ", " + COLUMN_TEXT + " text"
            + ", " + COLUMN_LAST_REPLIED_BY + " text"
            + ", " + COLUMN_IS_NEW + " bool default 0"
            + ", " + COLUMN_IS_NEW_CS + " bool default 0"
            + ");";

    public Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(ORDERS_CREATE);
        database.execSQL(MESSAGES_CREATE);
        database.execSQL(MSG_THREADS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);
        if (oldVersion == 1) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MSG_THREADS);
            db.execSQL(MESSAGES_CREATE);
            db.execSQL(MSG_THREADS_CREATE);
        }
    }
}
