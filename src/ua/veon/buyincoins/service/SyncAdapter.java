package ua.veon.buyincoins.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;
import org.apache.http.auth.AuthenticationException;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.controllers.SessionParser;
import ua.veon.buyincoins.db.DbManager;
import ua.veon.buyincoins.models.MessageThreadRecord;
import ua.veon.buyincoins.models.OrderHistoryRecord;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/27/13
 * Time: 6:33 PM
 */
class SyncAdapter extends AbstractThreadedSyncAdapter {
    private final String TAG = "SyncAdapter";
    private final Context context;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        Log.i(TAG, "SyncAdapter");
        this.context = context;
    }

    @Override
    public void onPerformSync(final Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        Log.i(TAG, "onPerformSync()");
        SessionParser parser = new SessionParser();
        AccountManager mAccountManager = AccountManager.get(context);
        try {
            parser.openSession(account.name, mAccountManager.getPassword(account));

            if (context.getString(R.string.authority_order).equals(authority)) {
                List<OrderHistoryRecord> records = parser.getOrdersHistory();
                syncResult.stats.numEntries += records.size();
                DbManager.upsertOrders(context, account.name, records);
            }

            if (context.getString(R.string.authority_message).equals(authority)) {
                List<MessageThreadRecord> records = parser.getMessagesHistory(syncResult.fullSyncRequested ? 5 : 1);
                syncResult.stats.numEntries += records.size();
                DbManager.upsertMessageThreads(context, account.name, records);
            }

            syncResult.delayUntil = 6 * 60 * 60; // 6 hours
        } catch (final ParseException e) {
            syncResult.stats.numParseExceptions++;
            Log.e(TAG, "ParseException", e);
        } catch (final AuthenticationException e) {
            syncResult.stats.numAuthExceptions++;
            Log.e(TAG, "AuthException", e);
        } catch (final IOException e) {
            syncResult.stats.numIoExceptions++;
            Log.e(TAG, "IOException", e);
        } catch (final Exception e) {
            Log.e(TAG, "onPerformSync", e);
        } finally {
            Log.d(TAG, "Finished OrderSync");
        }
    }
}
