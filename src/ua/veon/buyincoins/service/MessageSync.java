package ua.veon.buyincoins.service;

import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/25/13
 * Time: 1:35 PM
 */
public class MessageSync extends Service {
    private static final String TAG = "MessageSync";
    private static SyncAdapter mSyncAdapter = null;

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "inBind() " + intent.getAction());
        return getSyncAdapter().getSyncAdapterBinder();
    }

    private synchronized AbstractThreadedSyncAdapter getSyncAdapter() {
        Log.i(TAG, "getSyncAdapter()");
        if (mSyncAdapter == null) {
            mSyncAdapter = new SyncAdapter(this, true);
        }
        return mSyncAdapter;
    }
}


