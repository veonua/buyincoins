package ua.veon.buyincoins.utils;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/26/13
 * Time: 12:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class AsyncResult<D> {
    private Exception exception;
    private D data;

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

    public void setData(D data) {
        this.data = data;
    }

    public D getData() {
        return data;
    }

}
