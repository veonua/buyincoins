package ua.veon.buyincoins.controllers;

import android.content.Context;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.models.Status;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/24/13
 * Time: 11:46 PM
 */
public class StatusHelper {
    public static CharSequence toString(Context context, Status status) {
        if (status == null)
            return getStringArray(context)[0];
        return getStringArray(context)[status.ordinal()];
    }

    public static String[] getStringArray(Context context) {
        return context.getResources().getStringArray(R.array.order_status);
    }

    public static Status parseSite(String b) {
        String str = b.trim().toLowerCase(Locale.ENGLISH);

        if ("processing".equals(str)) return Status.PROCESS_0;
        if ("processing-1".equals(str)) return Status.PROCESS_1;
        if ("processing-2".equals(str)) return Status.PROCESS_2;
        if ("processing-3".equals(str)) return Status.PROCESS_3;
        if ("processing-4".equals(str)) return Status.PROCESS_4;
        if ("processing-5".equals(str)) return Status.PROCESS_5;

        if ("preparing for ship".equals(str)) return Status.PREP_FOR_SHIP;
        if ("waiting for dispatch".equals(str)) return Status.WAIT_FOR_DISPATCH;
        if ("split dispatching".equals(str)) return Status.SPLIT_DISPATCHING;
        if ("dispatched".equals(str)) return Status.DISPATCHED;
        if ("warning".equals(str)) return Status.WARNING;
        if ("complete".equals(str)) return Status.COMPLETE;
        if ("refund".equals(str)) return Status.REFUND;
        if ("cancelled".equals(str)) return Status.CANCELLED;

        return Status.UNKNOWN;
    }
}
