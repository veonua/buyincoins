package ua.veon.buyincoins.controllers;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/22/13
 * Time: 12:16 AM
 */
public class OrderException extends Exception {
    public OrderException(String s) {
        super(s);
    }
}
