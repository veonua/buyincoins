package ua.veon.buyincoins.controllers;

import android.text.TextUtils;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.htmlcleaner.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ua.veon.buyincoins.models.MessageRecord;
import ua.veon.buyincoins.models.MessageThreadRecord;
import ua.veon.buyincoins.models.OrderHistoryRecord;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/19/13
 * Time: 10:03 PM
 */
public class SessionParser implements Closeable {
    private final HtmlCleaner cleaner;
    private final HttpContext localContext;
    private static final String TAG = "SessionParser";

    public SessionParser() {
        CleanerProperties props = new CleanerProperties();

        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
        props.setRecognizeUnicodeChars(true);
        cleaner = new HtmlCleaner(props);

        CookieStore cookieStore = new BasicCookieStore();
        localContext = new BasicHttpContext();
        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
    }

    public void openSession(String user, String password) throws IOException, ParseException, AuthenticationException {
        if (TextUtils.isEmpty(user) || TextUtils.isEmpty(password))
            throw new AuthenticationException(user);


        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("http://www.buyincoins.com/index.php");
        HttpResponse ret = client.execute(get, localContext);
        HttpEntity entity = ret.getEntity();
        String secret = parseIndex(entity.getContent());
        entity.consumeContent();
        String json = postLoginInfo(user, password, secret);
        try {
            if (!parseLogin(json))
                throw new AuthenticationException(user);
        } catch (JSONException e) {
            Log.e(TAG, json, e);
            throw new ParseException(e.getMessage(), 0);
        }
    }

    public String postLoginInfo(String user, String password, String securityToken) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://www.buyincoins.com/index.php?main_page=login&action=process");

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("email_address", user));
        nameValuePairs.add(new BasicNameValuePair("extendField", "false"));
        nameValuePairs.add(new BasicNameValuePair("password", password));
        nameValuePairs.add(new BasicNameValuePair("securityToken", securityToken));
        nameValuePairs.add(new BasicNameValuePair("lang", "english"));

        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        HttpResponse resp = client.execute(post, localContext);
        HttpEntity ent = resp.getEntity();
        try {
            return EntityUtils.toString(ent);
        } finally {
            ent.consumeContent();
        }
    }

    public boolean parseLogin(String s) throws JSONException {
        Log.v(TAG, s);
        JSONObject root = new JSONObject(s);
        int result = root.getInt("r");
        return result == 1 && root.optInt("IS_LOGGED", 0) == 1;
    }

    public String parseIndex(InputStream stream) throws IOException, ParseException {
        TagNode root = cleaner.clean(stream);
        if (root == null)
            throw new NullPointerException("root tag");

        Object[] securityTokenNode;
        try {
            securityTokenNode = root.evaluateXPath("//body/div[@id='hidden']/input[@id='securityToken']");
            if (securityTokenNode == null || securityTokenNode.length < 1)
                throw new ParseException("securityTokenNode is not found", 0);
        } catch (XPatherException e) {
            throw new ParseException(e.getMessage(), 0);
        }
        return ((TagNode) securityTokenNode[0]).getAttributeByName("value");
    }

    public List<OrderHistoryRecord> getOrdersHistory() throws IOException, ParseException {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("http://www.buyincoins.com/index.php?main_page=account_history");
        HttpResponse response = client.execute(get, localContext);
        HttpEntity ent = response.getEntity();
        try {
            return parseOrderHistory(ent.getContent());
        } finally {
            ent.consumeContent();
        }
    }

    public List<MessageThreadRecord> getMessagesHistory(int pages_to_load) throws IOException, ParseException {
        int page = 1;
        int max;
        List<MessageThreadRecord> ret = new ArrayList<MessageThreadRecord>();
        do {
            ParseMessagesResult pp = getMessagesHistoryImpl(page++);
            max = pp.max_page;
            ret.addAll(pp.list);
        } while (page <= max && page <= pages_to_load);
        return ret;
    }

    /**
     * Returns message threads for given page as well as page num, page max
     *
     * @param page one-based index
     * @return message threads for given page
     * @throws IOException    connection error
     * @throws ParseException parser error
     */
    private ParseMessagesResult getMessagesHistoryImpl(int page) throws IOException, ParseException {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("http://www.buyincoins.com/index.php?main_page=account&ajax=1&action=get_msg&p=" + page + "&lang=english");
        HttpResponse response = client.execute(get, localContext);
        HttpEntity ent = response.getEntity();
        try {
            return parseMessagesHistory(EntityUtils.toString(ent));
        } catch (JSONException ex) {
            throw new ParseException(ex.getMessage(), 0);
        } finally {
            ent.consumeContent();
        }
    }

    public List<OrderHistoryRecord> parseOrderHistory(InputStream stream) throws IOException, ParseException {
        TagNode root = cleaner.clean(stream);
        if (root == null)
            throw new NullPointerException("root tag");

        Object[] orders;
        try {
            orders = root.evaluateXPath("//body/div[@class='bodyCont']/div[@class='cBox']/fieldset");
        } catch (XPatherException e) {
            throw new ParseException(e.getMessage(), 0);
        }
        if (orders == null || orders.length < 1) return null;

        SimpleDateFormat df = new SimpleDateFormat("EEEE dd MMMM, yyyy", Locale.ENGLISH); // Saturday 05 January, 2013

        List<OrderHistoryRecord> ret = new ArrayList<OrderHistoryRecord>(orders.length);
        for (Object o : orders)
            try {
                OrderHistoryRecord r = new OrderHistoryRecord();
                TagNode tn = (TagNode) o;
                r.id = tn.findElementByName("legend", false).getText().substring(14);
                r.status = tn.findElementByAttValue("class", "notice forward", false, false).getText().substring(14);
                List node = tn.findElementByAttValue("class", "content back", false, false).getChildren();
                String s = ((ContentNode) node.get(1)).getContent().toString().trim();
                r.date = df.parse(s);
                r.recipient = ((ContentNode) node.get(4)).getContent().toString().trim();

                node = tn.findElementByAttValue("class", "content", false, false).getChildren();
                r.productCount = Integer.parseInt(((ContentNode) node.get(1)).getContent().toString().trim());
                String price = ((ContentNode) node.get(4)).getContent().toString().trim();

                try {
                    r.price = NumberFormat.getCurrencyInstance(Locale.US).parse(price).doubleValue();
                } catch (ParseException ignore) {
                }

                r.link = tn.findElementByAttValue("class", "viewThisOrder", true, false).getAttributeByName("href");

                ret.add(r);
            } catch (Exception e) {
                Log.e(TAG, "parseHistory", e);
            }

        return ret;
    }

    @Override
    public void close() throws IOException {
    }

    public List<MessageRecord> parseThread(String input) throws JSONException, ParseException {
        JSONArray array = new JSONArray(input);
        int len = array.length();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH); //:"2012-04-26",

        ArrayList<MessageRecord> list = new ArrayList<MessageRecord>(len);
        for (int it = 0; it < len; it++) {
            JSONObject o = array.getJSONObject(it);
            MessageRecord mtr = new MessageRecord();
            mtr.text = o.getString("msg_content");
            mtr.create_time = df.parse(o.getString("msg_wrotten_time"));
            mtr.author = o.getString("msg_wrotten_by");
            mtr.author_type = "customer".equals(o.getString("msg_wrotten_type")) ? 0 : 1;
            list.add(mtr);
        }

        return list;
    }

    public ParseThreadResult getMessageThread(long threadId) throws IOException, ParseException {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet("http://www.buyincoins.com/index.php?main_page=account&ajax=1&action=get_msg_content&id=" + threadId + "&front=1&lang=english");
        HttpResponse response = client.execute(get, localContext);
        HttpEntity ent = response.getEntity();
        try {
            ParseThreadResult result = new ParseThreadResult();
            result.threadId = threadId;
            result.list = parseThread(EntityUtils.toString(ent));
            return result;
        } catch (JSONException ex) {
            throw new ParseException(ex.getMessage(), 0);
        } finally {
            ent.consumeContent();
        }
    }

    public static class ParseMessagesResult {
        public List<MessageThreadRecord> list;
        public int page;
        public int max_page;
    }

    public static class ParseThreadResult {
        public List<MessageRecord> list;
        public long threadId;
    }

    public ParseMessagesResult parseMessagesHistory(String input) throws JSONException, ParseException {
        JSONObject root = new JSONObject(input);
        JSONArray array = root.getJSONArray("msg");
        int len = array.length();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH); //:"2012-04-26",

        ArrayList<MessageThreadRecord> list = new ArrayList<MessageThreadRecord>(len);
        for (int it = 0; it < len; it++) {
            JSONObject o = array.getJSONObject(it);
            int id = o.getInt("msg_id");
            MessageThreadRecord mtr = new MessageThreadRecord(id);
            mtr.is_new_for_cs = !TextUtils.isEmpty(o.getString("msg_is_new_for_cs"));
            mtr.is_new = !TextUtils.isEmpty(o.getString("msg_is_new_for_customer"));
            mtr.subject = o.getString("msg_subject");
            //    "msg_create_by":"-1",
            //    "msg_create_by_name":" - %customer_name%",
            mtr.create_time = df.parse(o.getString("msg_create_time"));
            mtr.replied_time = df.parse(o.getString("msg_last_replied_time"));
            mtr.last_replied_by = o.getString("msg_last_replied_by");
            //    "msg_type":"4",
            //    "msg_url":"http:\/\/www.buyincoins.com\/index.php?main_page=account",
            //    "msg_manager":"Lisa Wang"
            list.add(mtr);
        }

        JSONObject page = root.getJSONObject("page");

        ParseMessagesResult res = new ParseMessagesResult();
        res.list = list;
        res.max_page = page.getInt("msg_total_page");
        res.page = page.getInt("msg_page_on");
        return res;
    }
}
