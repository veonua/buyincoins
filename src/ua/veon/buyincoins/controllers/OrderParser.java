package ua.veon.buyincoins.controllers;

import org.htmlcleaner.*;
import ua.veon.buyincoins.models.OrderDynamic;
import ua.veon.buyincoins.models.OrderSummary;
import ua.veon.buyincoins.models.Status;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/13/13
 * Time: 12:52 AM
 */
public class OrderParser {

    public static OrderSummary parse(InputStream stream, boolean isAjax) throws XPatherException, IOException, ParseException, OrderException {
        OrderSummary result = new OrderSummary();
        result.status = new OrderDynamic();

        CleanerProperties props = new CleanerProperties();

        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
        props.setRecognizeUnicodeChars(true);
        HtmlCleaner cleaner = new HtmlCleaner(props);
        TagNode root = cleaner.clean(stream);
        if (root == null)
            throw new NullPointerException("root tag");

        TagNode body = root.findElementByName("body", false);
        if (!isAjax)
            body = body.findElementByAttValue("class", "bodyCont", false, false);

        if (body == null)
            throw new NullPointerException("root tag");

        if (body.getChildren().size() == 1)
            throw new OrderException(body.getChildren().get(0).toString());

        Object[] orderStatusObjects = body.findElementByAttValue("class", "orderStatus", true, true).evaluateXPath("//p");
        if (orderStatusObjects == null || orderStatusObjects.length < 1)
            throw new IllegalStateException("orderStatus tag");

        TagNode[] orderStatusNodes = new TagNode[orderStatusObjects.length];
        short _it = 0;
        for (Object o : orderStatusObjects) {
            orderStatusNodes[_it++] = (TagNode) o;
        }

        result.status.status_text = orderStatusNodes[0].findElementByName("b", false).getText().toString();
        result.status.status_code = StatusHelper.parseSite(result.status.status_text);

        short node_it = 1;
        do {
            if (orderStatusNodes.length <= node_it) break;
            if ("messageStackError".equals(orderStatusNodes[node_it].getAttributeByName("class"))) {
                result.status.orderWarning = orderStatusNodes[node_it].getText().toString();
                node_it++;
            }

            if (orderStatusNodes.length <= node_it) break;
            if (result.status.status_code.compareTo(Status.SPLIT_DISPATCHING) >= 0)
                result.status.setTrackingNumbers(((ContentNode) orderStatusNodes[node_it++].getChildren().get(1)).getContent());

            if (orderStatusNodes.length <= node_it) break;
            result.courierService = ((ContentNode) orderStatusNodes[node_it].getChildren().get(1)).getContent().toString();
        } while (false);

        Object[] orderTotals = body.evaluateXPath("//div/table[@id='orderTotals']/tbody/tr");

        result.totals = new HashMap<String, String>();

        for (Object o : orderTotals) {
            TagNode t = (TagNode) o;
            List ch = t.getChildren();
            String key = ((TagNode) ch.get(0)).getText().toString().trim();
            String value = ((TagNode) ch.get(1)).getText().toString().trim();
            result.totals.put(key, value);
        }

        TagNode deliverAddr = body.findElementByAttValue("class", "deliverAddr cBox l", false, true);

        StringBuffer tmp = deliverAddr.findElementByAttValue("class", "forward", false, false).getText();
        String sub = tmp.substring(12);
        SimpleDateFormat df = new SimpleDateFormat("EEEE dd MMMM, yyyy", Locale.ENGLISH); // Saturday 05 January, 2013
        result.orderDate = df.parse(sub);
        result.deliveryAddress = deliverAddr.findElementByName("address", false).getText().toString();
        result.paymentMethod = ((ContentNode) ((TagNode) deliverAddr.getElementListByName("p", false).get(1)).getChildren().get(1)).getContent().toString();

        return result;
    }

}
