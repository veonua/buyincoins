package ua.veon.buyincoins.loader;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import ua.veon.buyincoins.controllers.SessionParser;
import ua.veon.buyincoins.utils.AsyncResult;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/12/13
 * Time: 10:33 PM
 */
public class ThreadLoader extends AsyncTaskLoader<AsyncResult<SessionParser.ParseThreadResult>> {
    private final long threadId;
    private final Account account;

    public ThreadLoader(Context context, long threadId, Account account) {
        super(context);
        if (context == null)
            throw new NullPointerException("context");
        if (threadId == 0)
            throw new IllegalArgumentException("threadId");
        if (account == null)
            throw new NullPointerException("account");

        this.threadId = threadId;
        this.account = account;
    }

    @Override
    public AsyncResult<SessionParser.ParseThreadResult> loadInBackground() {
        Context context = getContext();
        AccountManager accountManager = AccountManager.get(context);
        String pass = accountManager.getPassword(account);
        SessionParser sp = new SessionParser();

        AsyncResult<SessionParser.ParseThreadResult> result = new AsyncResult<SessionParser.ParseThreadResult>();

        try {
            sp.openSession(account.name, pass);
            result.setData(sp.getMessageThread(threadId));
        } catch (Exception e) {
            result.setException(e);
        }

        return result;
    }
}
