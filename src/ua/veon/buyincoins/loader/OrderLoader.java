package ua.veon.buyincoins.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import ua.veon.buyincoins.controllers.OrderParser;
import ua.veon.buyincoins.models.OrderSummary;
import ua.veon.buyincoins.utils.AsyncResult;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/12/13
 * Time: 10:33 PM
 */
public class OrderLoader extends AsyncTaskLoader<AsyncResult<OrderSummary>> {
    private final String username;
    private final String orderNum;

    public OrderLoader(Context context, String email, String orderNum) {
        super(context);
        this.username = email;
        this.orderNum = orderNum;
    }

    @Override
    public AsyncResult<OrderSummary> loadInBackground() {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://www.buyincoins.com/ajax.php?act=trackOrder&ajax=1&o=" + orderNum + "&email=" + username);

        AsyncResult<OrderSummary> result = new AsyncResult<OrderSummary>();

        try {
            HttpResponse response = client.execute(request);
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new IOException("Invalid response from server: " + status.toString());
            }

            InputStream stream = response.getEntity().getContent();
            try {
                result.setData(OrderParser.parse(stream, true));
            } finally {
                stream.close();
            }
        } catch (Exception e) {
            result.setException(e);
        }

        return result;
    }
}
