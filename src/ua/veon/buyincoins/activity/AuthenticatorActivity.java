/*
 * Copyright (C) 2010 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package ua.veon.buyincoins.activity;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.controllers.SessionParser;
import ua.veon.buyincoins.db.MessageContentProvider;
import ua.veon.buyincoins.db.OrderContentProvider;

/**
 * Activity which displays login screen to the user.
 */
@SuppressWarnings("deprecation")
public class AuthenticatorActivity extends AccountAuthenticatorActivity {
    public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";

    private static final String TAG = "AuthenticatorActivity";

    private AccountManager mAccountManager;
    private Thread mAuthThread;
    private String mAuthtokenType;
    private ToastReport toastReport;

    /**
     * If set we are just checking that the user knows their credentials; this
     * doesn't cause the user's password to be changed on the device.
     */
    private Boolean mConfirmCredentials = false;

    /**
     * for posting authentication attempts back to UI thread
     */
    private final Handler mHandler = new Handler();
    private String mPassword;
    private EditText mPasswordEdit;

    /**
     * Was the original caller asking for an entirely new account?
     */
    protected boolean mRequestNewAccount = false;

    private String mUsername;
    private EditText mUsernameEdit;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle icicle) {
        Log.i(TAG, "onCreate(" + icicle + ")");
        super.onCreate(icicle);
        mAccountManager = AccountManager.get(this);
        Log.i(TAG, "loading data from Intent");
        final Intent intent = getIntent();
        mUsername = intent.getStringExtra(PARAM_USERNAME);
        mAuthtokenType = intent.getStringExtra(PARAM_AUTHTOKEN_TYPE);
        mRequestNewAccount = mUsername == null;
        mConfirmCredentials =
                intent.getBooleanExtra(PARAM_CONFIRMCREDENTIALS, false);

        Log.i(TAG, "    request new: " + mRequestNewAccount);

        requestWindowFeature(android.view.Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.signin);

        mUsernameEdit = (EditText) findViewById(R.id.username_edit);
        mPasswordEdit = (EditText) findViewById(R.id.password_edit);

        mUsernameEdit.setText(mUsername);
        toastReport = new ToastReport(this, findViewById(R.id.toastbar));
    }

    //
    // {@inheritDoc}
    //
    @Override
    protected Dialog onCreateDialog(int id) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getText(R.string.wait));
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Log.i(TAG, "dialog cancel has been invoked");
                if (mAuthThread != null) {
                    mAuthThread.interrupt();
                    finish();
                }
            }
        });
        return dialog;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        toastReport.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        toastReport.onRestoreInstanceState(savedInstanceState);
    }

    //
    // Handles onClick event on the Submit button. Sends username/password to
    // the server for authentication.
    //
    // @param view The Submit button for which this method is invoked
    //
    @SuppressWarnings("UnusedParameters")
    public void handleLogin(View view) {
        if (mRequestNewAccount) {
            mUsername = mUsernameEdit.getText().toString();
        }
        mPassword = mPasswordEdit.getText().toString();
        if (TextUtils.isEmpty(mUsername) || TextUtils.isEmpty(mPassword)) {
            Toast.makeText(this, R.string.error_fill_fields, Toast.LENGTH_SHORT).show();
        } else {
            showProgress();
            // Start authenticating...
            mAuthThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    SessionParser sp = new SessionParser();
                    Throwable error = null;
                    try {
                        sp.openSession(mUsername, mPassword);
                    } catch (Exception e) {
                        Log.e(TAG, "checker", e);
                        error = e;
                    }

                    final Throwable finalResult = error;
                    mHandler.post(new Runnable() {
                        public void run() {
                            AuthenticatorActivity.this.onAuthenticationResult(finalResult);
                        }
                    });
                }
            };
            mAuthThread.start();
        }
    }

    //
    // Called when response is received from the server for confirm credentials
    //  request. See onAuthenticationResult(). Sets the
    // AccountAuthenticatorResult which is sent back to the caller.
    //
    // @param the confirmCredentials result.
    //
    protected void finishConfirmCredentials(boolean result) {
        Log.i(TAG, "finishConfirmCredentials()");
        final Account account = new Account(mUsername, getString(R.string.account_type));
        mAccountManager.setPassword(account, mPassword);
        final Intent intent = new Intent();
        intent.putExtra(AccountManager.KEY_BOOLEAN_RESULT, result);
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    //
    //
    // Called when response is received from the server for authentication
    // request. See onAuthenticationResult(). Sets the
    // AccountAuthenticatorResult which is sent back to the caller. Also sets
    // the authToken in AccountManager for this account.
    //
    // @param the confirmCredentials result.
    //

    protected void finishLogin() {
        String account_type = getString(R.string.account_type);
        final Account account = new Account(mUsername, account_type);

        if (mRequestNewAccount) {
            mAccountManager.addAccountExplicitly(account, mPassword, null);
            // Set contacts sync for this account.
            ContentResolver.setSyncAutomatically(account,
                    OrderContentProvider.AUTHORITY, true);

            ContentResolver.setSyncAutomatically(account,
                    MessageContentProvider.AUTHORITY, true);
        } else {
            mAccountManager.setPassword(account, mPassword);
        }
        final Intent intent = new Intent();
        String mAuthtoken = mPassword;
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account_type);
        if (mAuthtokenType != null
                && OrderContentProvider.AUTHORITY.equals(mAuthtokenType)) {
            intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
        }
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
    }

    //*
    // Hides the progress UI for a lengthy operation.
    //
    protected void hideProgress() {
        setProgressBarIndeterminateVisibility(false);
        try {
            dismissDialog(99);
        } catch (Exception ignore) {
        }

    }

    //
    // Called when the authentication process completes (see attemptLogin()).
    //
    public void onAuthenticationResult(Throwable th) {
        // Hide the progress dialog
        hideProgress();
        if (th == null) {
            if (!mConfirmCredentials) {
                finishLogin();
            } else {
                finishConfirmCredentials(true);
            }
        } else {
            Log.e(TAG, "onAuthenticationResult: failed to authenticate", th);
            if (mRequestNewAccount) {
                // "Please enter a valid username/password.
                toastReport.showBar(false, th);
            } else {
                // "Please enter a valid password." (Used when the
                // account is already in the database but the password
                // doesn't work.)
                toastReport.showBar(false, th);
            }
        }
    }

    //
    // Shows the progress UI for a lengthy operation.
    //
    protected void showProgress() {
        setProgressBarIndeterminateVisibility(true);
        showDialog(99);
    }
}
