package ua.veon.buyincoins.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.flurry.android.FlurryAgent;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.controllers.StatusHelper;
import ua.veon.buyincoins.db.DbManager;
import ua.veon.buyincoins.loader.OrderLoader;
import ua.veon.buyincoins.models.OrderLocalModel;
import ua.veon.buyincoins.utils.AsyncResult;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.Date;

public class OrderSummary extends SherlockFragmentActivity {

    public static final String EMAIL = "email";
    public static final String ORDER_ID = "order_id";
    private static final String TAG = "buyincoins.activity.OrderSummary";
    private SummaryFragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP, ActionBar.DISPLAY_HOME_AS_UP);
        setSupportProgressBarIndeterminateVisibility(false);

        setTitle(getIntent().getExtras().getString(ORDER_ID));
        fragment = (SummaryFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);

        if (fragment == null) {
            fragment = SummaryFragment.newInstance(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Main.FLURRY);
    }

    @Override
    protected void onStop() {
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragment != null)
            fragment.onSave();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public static class SummaryFragment extends SherlockFragment implements LoaderManager.LoaderCallbacks<AsyncResult<ua.veon.buyincoins.models.OrderSummary>> {
        private OrderLocalModel model;

        public SummaryFragment() {
            super();
        }

        public static SummaryFragment newInstance(Bundle bundle) {
            SummaryFragment ret = new SummaryFragment();
            ret.setArguments(bundle);
            return ret;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.order_summary, null);

            try {
                model = DbManager.getOrderById(getActivity(), getArguments().getString(ORDER_ID));
            } catch (IndexOutOfBoundsException ex) {
                model = new OrderLocalModel(getArguments().getString(ORDER_ID));
            } catch (Exception ex) {
                Log.e(TAG, "onCreateView", ex);
                getActivity().finish();
                return null;
            }

            updateViews(v);

            setHasOptionsMenu(true);
            return v;
        }

        private void updateViews(View v) {
            if (model == null) throw new IllegalStateException("model");
            Context context = getActivity().getApplicationContext();
            String header = getString(R.string.status_header, StatusHelper.toString(context, model.getStatus()));
            ((TextView) v.findViewById(R.id.status)).setText(header);

            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
            Date date = model.getDate();
            if (date != null) {
                v.findViewById(R.id.date_group).setVisibility(View.VISIBLE);
                ((TextView) v.findViewById(R.id.date)).setText(dateFormat.format(date));
            } else {
                v.findViewById(R.id.date_group).setVisibility(View.GONE);
            }


            ((EditText) v.findViewById(R.id.comment)).setText(model.getComment());

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getSherlockActivity().getSupportLoaderManager().initLoader(0, getArguments(), this).forceLoad();
        }

        public void addTrack(String str) {
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("trackchecker://tracks/add?n=" + URLEncoder.encode(str, "UTF-8"))));
            } catch (Exception e) {
                Toast.makeText(getActivity(), R.string.error_no_trackcheck, Toast.LENGTH_LONG).show();
                Log.e(TAG, "addTrack", e);
            }
        }

        @Override
        public Loader<AsyncResult<ua.veon.buyincoins.models.OrderSummary>> onCreateLoader(int i, Bundle bundle) {
            getSherlockActivity().setSupportProgressBarIndeterminateVisibility(true);
            return new OrderLoader(getActivity().getApplicationContext(), bundle.getString(EMAIL), bundle.getString(ORDER_ID));
        }

        @Override
        public void onLoadFinished(Loader<AsyncResult<ua.veon.buyincoins.models.OrderSummary>> objectLoader, AsyncResult<ua.veon.buyincoins.models.OrderSummary> res) {
            getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
            View v = getView();
            Context context = getActivity().getApplicationContext();

            ua.veon.buyincoins.models.OrderSummary o = res.getData();
            v.findViewById(android.R.id.progress).setVisibility(View.GONE);
            @SuppressWarnings("ThrowableResultOfMethodCallIgnored") Exception ex = res.getException();
            if (ex != null || o == null) {
                if (ex != null)
                    Toast.makeText(context, ex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(context, R.string.error_order_null, Toast.LENGTH_LONG).show();

                getActivity().finish();
                return;
            }

            ((TextView) v.findViewById(R.id.warning)).setText(o.status.orderWarning);

            String[] trackingNumbers = o.status.getTrackingNumbers();

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout tracking = (LinearLayout) v.findViewById(R.id.tracking);
            if (trackingNumbers != null) {
                v.findViewById(R.id.tracking_group).setVisibility(View.VISIBLE);
                tracking.removeAllViewsInLayout();

                for (final String s : trackingNumbers) {
                    View view = layoutInflater.inflate(R.layout.track_row, null);
                    ((TextView) view.findViewById(android.R.id.text1)).setText(s);
                    view.findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addTrack(s);
                        }
                    });
                    tracking.addView(view);
                }
            }

            if (!TextUtils.isEmpty(o.courierService)) {
                v.findViewById(R.id.courier_group).setVisibility(View.VISIBLE);
                ((TextView) v.findViewById(R.id.courier)).setText(o.courierService);
            }

            model.setDate(o.orderDate);
            model.setStatus(o.status.status_code, o.status.status_text);

            updateViews(v);

            v.findViewById(R.id.address_group).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.address)).setText(o.deliveryAddress);
            v.findViewById(R.id.payment_group).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.payment_method)).setText(o.paymentMethod);

        }

        @Override
        public void onLoaderReset(Loader<AsyncResult<ua.veon.buyincoins.models.OrderSummary>> objectLoader) {
            Log.i(TAG, "LoaderReset");
        }

        public void onSave() {
            Bundle bundle = getArguments();
            model.setComment(((EditText) getView().findViewById(R.id.comment)).getText().toString());
            DbManager.upsertOrder(getActivity(), bundle.getString(EMAIL), model);
        }
    }
}
