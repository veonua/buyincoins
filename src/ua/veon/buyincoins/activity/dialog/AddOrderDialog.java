package ua.veon.buyincoins.activity.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import com.actionbarsherlock.app.SherlockDialogFragment;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.activity.OrderSummary;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/19/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddOrderDialog extends SherlockDialogFragment {
    public static final String TAG = "AddOrderDialog";

    public AddOrderDialog() {
        super();
    }

    public static SherlockDialogFragment newInstance() {
        return new AddOrderDialog();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View v = getContentView();

        Dialog dialog = new AlertDialog.Builder(getSherlockActivity())
                .setTitle(R.string.addOrder)
                .setView(v)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String email = ((EditText) v.findViewById(android.R.id.text1)).getText().toString();
                        String orderId = "B" + ((EditText) v.findViewById(android.R.id.text2)).getText().toString();
                        Intent sum = new Intent(getActivity().getApplicationContext(), OrderSummary.class);
                        sum.putExtra(OrderSummary.EMAIL, email);
                        sum.putExtra(OrderSummary.ORDER_ID, orderId);
                        startActivity(sum);
                    }
                }).create();

        return dialog;
    }

    public View getContentView() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.add_order, null);
        SharedPreferences sp = getActivity().getPreferences(Context.MODE_PRIVATE);
        ((EditText) v.findViewById(android.R.id.text1)).setText(sp.getString(OrderSummary.EMAIL, ""));
        return v;
    }
}
