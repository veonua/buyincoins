package ua.veon.buyincoins.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ApplicationErrorReport;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.auth.AuthenticationException;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.utils.Utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/21/13
 * Time: 9:11 PM
 */

@TargetApi(11)
public class ToastReport {
    private final View mBarView;
    private final TextView mMessageView;

    private ViewPropertyAnimator mBarAnimator;
    private Handler mHideHandler = new Handler();
    private final Context context;

    // State objects
    private Throwable error;
    private int messageRes;

    @TargetApi(11)
    public ToastReport(final Context context, View view) {
        this.context = context;
        mBarView = view;
        if (Utils.hasHoneycomb())
            mBarAnimator = mBarView.animate();

        mMessageView = (TextView) mBarView.findViewById(R.id.error_message);
        mBarView.findViewById(R.id.report_button)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideBar(false);
                        ApplicationErrorReport report = new ApplicationErrorReport();
                        report.packageName = report.processName = context.getApplicationContext().getPackageName();
                        report.time = System.currentTimeMillis();
                        report.type = ApplicationErrorReport.TYPE_CRASH;

                        ApplicationErrorReport.CrashInfo crash = new ApplicationErrorReport.CrashInfo();
                        crash.exceptionClassName = error.getClass().getSimpleName();
                        crash.exceptionMessage = error.getMessage();

                        StringWriter writer = new StringWriter();
                        PrintWriter printer = new PrintWriter(writer);
                        error.printStackTrace(printer);

                        crash.stackTrace = writer.toString();

                        StackTraceElement stack = error.getStackTrace()[0];
                        crash.throwClassName = stack.getClassName();
                        crash.throwFileName = stack.getFileName();
                        crash.throwLineNumber = stack.getLineNumber();
                        crash.throwMethodName = stack.getMethodName();

                        report.crashInfo = crash;

                        Intent intent = new Intent(Intent.ACTION_APP_ERROR);
                        intent.putExtra(Intent.EXTRA_BUG_REPORT, report);
                        context.startActivity(intent);
                    }
                });

        hideBar(true);
    }

    @TargetApi(11)
    public void showBar(boolean immediate, Throwable error) {
        boolean trivial;
        if (error instanceof IOException) {
            messageRes = R.string.error_connection;
            trivial = true;
        } else if (error instanceof ParseException) {
            messageRes = R.string.error_parse;
            trivial = false;
        } else if (error instanceof AuthenticationException) {
            messageRes = R.string.error_password;
            trivial = true;
        } else {
            messageRes = R.string.error_unknown;
            trivial = false;
        }

        if (trivial) {
            Toast.makeText(context, messageRes, Toast.LENGTH_SHORT).show();
            return;
        }

        this.error = error;
        mMessageView.setText(messageRes);

        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable,
                mBarView.getResources().getInteger(R.integer.toast_bar_hide_delay));

        mBarView.setVisibility(View.VISIBLE);
        if (Utils.hasHoneycomb()) {
            if (immediate || mBarAnimator == null) {
                mBarView.setAlpha(1);
            } else {
                mBarAnimator.cancel();
                mBarAnimator
                        .alpha(1)
                        .setDuration(
                                mBarView.getResources()
                                        .getInteger(android.R.integer.config_shortAnimTime))
                        .setListener(null);
            }
        }
    }

    @TargetApi(11)
    public void hideBar(boolean immediate) {
        mHideHandler.removeCallbacks(mHideRunnable);
        if (immediate || mBarAnimator == null) {
            mBarView.setVisibility(View.GONE);
            if (Utils.hasHoneycomb())
                mBarView.setAlpha(0);
            messageRes = 0;
            error = null;

        } else {
            mBarAnimator.cancel();
            mBarAnimator
                    .alpha(0)
                    .setDuration(mBarView.getResources()
                            .getInteger(android.R.integer.config_shortAnimTime))
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mBarView.setVisibility(View.GONE);
                            messageRes = 0;
                            error = null;
                        }
                    });
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("message", messageRes);
        outState.putSerializable("error", error);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            messageRes = savedInstanceState.getInt("message");
            error = (Throwable) savedInstanceState.getSerializable("error");

            if (error != null) {
                showBar(true, error);
            }
        }
    }

    private Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hideBar(false);
        }
    };
}

