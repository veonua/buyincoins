package ua.veon.buyincoins.activity.main;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.activity.Main;
import ua.veon.buyincoins.activity.OrderSummary;
import ua.veon.buyincoins.controllers.StatusHelper;
import ua.veon.buyincoins.db.DbManager;
import ua.veon.buyincoins.models.Status;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 2/2/13
 * Time: 1:38 AM
 */
public class OrdersFragment extends SherlockListFragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG = "OrdersFragment";
    private CursorAdapter adapter;

    public OrdersFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.order_list, null);

        v.findViewById(android.R.id.button2).setOnClickListener(this);

        v.findViewById(R.id.sign_in).setOnClickListener(this);
        v.findViewById(R.id.sync_now).setOnClickListener(this);

        setHasOptionsMenu(true);
        return v;
    }

    public void onClick(View view) {
        ((Main) getActivity()).onCommand(view.getId());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.order_list, menu);
    }

    /*
        private final ActionMode.Callback selectedActions = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                android.view.MenuInflater inflater = actionMode.getMenuInflater();
                inflater.inflate(R.menu.order_selected, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.remove: {
                        try {
                            long[] array = getListView().getCheckedItemIds();
                            ArrayList<String> strings = new ArrayList<String>(array.length);
                            for (long id : array) {
                                Cursor c = (Cursor) adapter.getItem((int) id);
                                strings.add(c.getString(0));
                            }
                            OrderDb db = new OrderDb(getActivity().getApplicationContext());
                            db.Delete(strings);
                            actionMode.finish();
                        } catch (Exception e) {
                            Log.e(TAG, "remove", e);
                        }
                    }
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
            }
        };
        */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        final Cursor c = (Cursor) adapter.getItem(position);

        Intent sum = new Intent(getActivity().getApplicationContext(), OrderSummary.class);
        sum.putExtra(OrderSummary.EMAIL, c.getString(1));
        sum.putExtra(OrderSummary.ORDER_ID, c.getString(0));
        startActivity(sum);

    }

    private static class MyCursorAdapter extends CursorAdapter {

        private class Holder {
            TextView title, date;
            TextView status;
        }

        private final LayoutInflater inflater;
        private final SimpleDateFormat df;
        private final String[] statuses;

        public MyCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            inflater = LayoutInflater.from(context);
            df = new SimpleDateFormat("d MMMM");
            statuses = StatusHelper.getStringArray(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View view = inflater.inflate(R.layout.order_list_row, viewGroup, false);
            MyCursorAdapter.Holder h = new MyCursorAdapter.Holder();
            h.title = (TextView) view.findViewById(android.R.id.text1);
            h.date = (TextView) view.findViewById(R.id.date);
            h.status = (TextView) view.findViewById(R.id.status);
            view.setTag(h);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            MyCursorAdapter.Holder h = (MyCursorAdapter.Holder) view.getTag();
            h.title.setText(cursor.getString(0));
            h.date.setText(df.format(new java.util.Date(cursor.getLong(2))));
            int st = cursor.getInt(3);
            if (st > 0 && st <= Status.CANCELLED.ordinal()) {
                h.status.setText(statuses[st]);
                h.status.setVisibility(View.VISIBLE);
            } else {
                h.status.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Context context = getActivity();
        adapter = new MyCursorAdapter(context, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setListAdapter(adapter);
        getLoaderManager().initLoader(0, null, this);

//            getListView().setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
//            getListView().setMultiChoiceModeListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        switch (i) {
            case 0:
                return DbManager.getOrdersLoader(getActivity());
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        try {
            if (cursor.isClosed()) return;

            if (cursorLoader.getId() == 0) {
                adapter.swapCursor(cursor);
            }
        } catch (Exception ignore) {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        if (cursorLoader.getId() == 0) adapter.swapCursor(null);
    }

    @Override
    public void onResume() {
        View v = getView();
        boolean hasAccount = ((Main) getActivity()).hasAccount();
        v.findViewById(R.id.sign_in).setVisibility(hasAccount ? View.GONE : View.VISIBLE);
        v.findViewById(R.id.sync_now).setVisibility(!hasAccount ? View.GONE : View.VISIBLE);
        super.onResume();
    }
}
