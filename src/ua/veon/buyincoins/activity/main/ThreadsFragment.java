package ua.veon.buyincoins.activity.main;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockListFragment;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.activity.MessageThread;
import ua.veon.buyincoins.db.DbManager;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 2/2/13
 * Time: 1:38 AM
 */
public class ThreadsFragment extends SherlockListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG = "ThreadsFragment";
    private CursorAdapter adapter;

    public ThreadsFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.theads_list, null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity().getApplicationContext(), MessageThread.class);
        intent.putExtra(BaseColumns._ID, id);
        Cursor c = (Cursor) adapter.getItem(position);
        intent.putExtra(Intent.EXTRA_TITLE, c.getString(1));
        startActivity(intent);
    }

    private static class MyCursorAdapter extends CursorAdapter {

        private class Holder {
            TextView subject, date;
            TextView author;
        }

        private final LayoutInflater inflater;
        private final SimpleDateFormat df;

        public MyCursorAdapter(Context context, Cursor c, int flags) {
            super(context, c, flags);
            inflater = LayoutInflater.from(context);
            df = new SimpleDateFormat("d MMM");
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            View view = inflater.inflate(R.layout.threads_row, viewGroup, false);
            MyCursorAdapter.Holder h = new MyCursorAdapter.Holder();
            h.subject = (TextView) view.findViewById(android.R.id.text1);
            h.date = (TextView) view.findViewById(R.id.date);
            h.author = (TextView) view.findViewById(R.id.author);
            view.setTag(h);
            return view;
        }

        @Override
        // BaseColumns._ID, Helper.COLUMN_TEXT, Helper.COLUMN_CHANGE_DATE, Helper.COLUMN_LAST_REPLIED_BY, Helper.COLUMN_IS_NEW, Helper.COLUMN_IS_NEW_CS };
        public void bindView(View view, Context context, Cursor cursor) {
            MyCursorAdapter.Holder h = (MyCursorAdapter.Holder) view.getTag();
            h.subject.setText(cursor.getString(1));
            boolean is_new = cursor.getInt(4) != 0;
            h.subject.setTextAppearance(context, is_new ? R.style.ThreadSubjectUnread : R.style.ThreadSubject);
            h.date.setText(df.format(new java.util.Date(cursor.getLong(2))));
            h.author.setText(cursor.getString(3));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Context context = getActivity();
        adapter = new MyCursorAdapter(context, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setListAdapter(adapter);
        getLoaderManager().initLoader(0, null, this);

//            getListView().setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
//            getListView().setMultiChoiceModeListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        switch (i) {
            case 0:
                return DbManager.getMessagesLoader(getActivity());
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        try {
            if (cursor.isClosed()) return;

            if (cursorLoader.getId() == 0) {
                adapter.swapCursor(cursor);
            }
        } catch (Exception ignore) {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        if (cursorLoader.getId() == 0) adapter.swapCursor(null);
    }
}
