package ua.veon.buyincoins.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ApplicationErrorReport;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncStatusObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.ArrayAdapter;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.activity.dialog.AddOrderDialog;
import ua.veon.buyincoins.activity.main.OrdersFragment;
import ua.veon.buyincoins.activity.main.ThreadsFragment;
import ua.veon.buyincoins.utils.Utils;

public class Main extends SherlockFragmentActivity implements SyncStatusObserver, ActionBar.OnNavigationListener {
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG = "Main";
    public static final String FLURRY = "VF6BRPJWCZSZB4BWC7V7";
    private static final int AUTH_ACTIVITY = 1000;
    private String AUTHORITY_ORDER;
    private String AUTHORITY_MESSAGE;
    private Object contentProviderHandle;
    private int currentPage = 0;
    private String currentAuth;
    private Handler progressWheelHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            setSupportProgressBarIndeterminateVisibility(msg.what != 0);
        }
    };
    private Account account = null;

    public boolean hasAccount() {
        return account != null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(com.actionbarsherlock.view.Window.FEATURE_INDETERMINATE_PROGRESS);

        AUTHORITY_ORDER = getString(R.string.authority_order);
        AUTHORITY_MESSAGE = getString(R.string.authority_message);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        Context context = actionbar.getThemedContext();
        ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(
                context, R.array.tabs, R.layout.sherlock_spinner_item);
        list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
        actionbar.setListNavigationCallbacks(list, this);
        actionbar.setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, FLURRY);
    }

    @Override
    protected void onStop() {
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    //private ToastReport toastReport;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentPage", currentPage);
        //toastReport.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        currentPage = savedInstanceState.getInt("currentPage");
        getSupportActionBar().setSelectedNavigationItem(currentPage);
    }

    public void startSync() {
        if (account != null) {
            Bundle params = new Bundle();
            params.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            params.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            ContentResolver.requestSync(account, currentAuth, new Bundle());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case AUTH_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    loadAccount();
                    startSync();
                    invalidateOptionsMenu();
                    break;
                }
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        ContentResolver.removeStatusChangeListener(contentProviderHandle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        contentProviderHandle = ContentResolver.addStatusChangeListener(
                ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE, this);
        loadAccount();
        onStatusChanged(0);
    }

    private void loadAccount() {
        AccountManager accountManager = AccountManager.get(this);
        Account[] accounts = accountManager.getAccountsByType(getString(R.string.account_type));

        if (accounts.length <= 0) {
            account = null;
            return;
        }
        account = accounts[0];
    }

    @Override
    public void onStatusChanged(int i) {
        boolean inProgress;

        inProgress = (ContentResolver.isSyncActive(account, currentAuth) || ContentResolver.isSyncPending(account, currentAuth));

        progressWheelHandler.sendEmptyMessage(inProgress ? 1 : 0);
    }

    public boolean onCommand(int command) {
        switch (command) {
            case R.id.sign_in: {
                Intent intent = new Intent(getApplicationContext(), AuthenticatorActivity.class);
                startActivityForResult(intent, Main.AUTH_ACTIVITY);
                return true;
            }
            case android.R.id.button2: {
                AddOrderDialog.newInstance().show(getSupportFragmentManager(), AddOrderDialog.TAG);
                return true;
            }

            case R.id.sync_now: {
                startSync();
                return true;
            }

            case R.id.bug: {
                ApplicationErrorReport report = new ApplicationErrorReport();
                report.packageName = report.processName = getApplicationContext().getPackageName();
                report.time = System.currentTimeMillis();
                report.type = ApplicationErrorReport.TYPE_CRASH;
                if (Utils.hasHoneycombMR1())
                    report.systemApp = false;

                ApplicationErrorReport.CrashInfo crash = new ApplicationErrorReport.CrashInfo();
                crash.exceptionClassName = "Feedback";
                crash.exceptionMessage = "Report a problem";
                report.crashInfo = crash;

                Intent intent = new Intent(Intent.ACTION_APP_ERROR);
                intent.putExtra(Intent.EXTRA_BUG_REPORT, report);
                startActivity(intent);
                return true;
            }

            case R.id.feedback: {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/spreadsheet/gform?key=0AjZVAt1pZIgydGl1bDJZLVl0UzUza3ZuQlZxdmYyUlE"));
                startActivity(browserIntent);
                return true;
            }

            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean hasAccount = hasAccount();
        menu.findItem(R.id.sync_now).setVisible(hasAccount);
        menu.findItem(R.id.sign_in).setVisible(!hasAccount);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return onCommand(item.getItemId());
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        Fragment fragment;
        switch (itemPosition) {
            case 0:
                fragment = new OrdersFragment();
                currentAuth = AUTHORITY_ORDER;
                break;
            case 1:
                fragment = new ThreadsFragment();
                currentAuth = AUTHORITY_MESSAGE;
                break;
            default:
                throw new IllegalArgumentException("item");
        }
        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentById(android.R.id.content) == null)
            fm.beginTransaction().add(android.R.id.content, fragment).commit();
        else
            fm.beginTransaction().replace(android.R.id.content, fragment).commit();

        currentPage = itemPosition;
        onStatusChanged(0);
        return true;
    }
}
