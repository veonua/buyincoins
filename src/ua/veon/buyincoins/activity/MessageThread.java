package ua.veon.buyincoins.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListFragment;
import ua.veon.buyincoins.R;
import ua.veon.buyincoins.controllers.SessionParser;
import ua.veon.buyincoins.db.DbManager;
import ua.veon.buyincoins.loader.ThreadLoader;
import ua.veon.buyincoins.utils.AsyncResult;

import java.text.SimpleDateFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 2/5/13
 * Time: 3:14 PM
 */
@SuppressWarnings("ALL")
public class MessageThread extends SherlockFragmentActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentById(android.R.id.content) == null) {
            MessageThreadFragment mtf = new MessageThreadFragment();
            Bundle bundle = getIntent().getExtras();
            mtf.setArguments(bundle);
            fm.beginTransaction().add(android.R.id.content, mtf).commit();
        }
        setTitle(getIntent().getStringExtra(Intent.EXTRA_TITLE));
    }

    public static class MessageThreadFragment extends SherlockListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
        private static final String TAG = "MessageThreadFragment";
        private CursorAdapter adapter;

        public MessageThreadFragment() {
            super();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.message_thread, null);
        }

        private static class MyCursorAdapter extends CursorAdapter {

            private class Holder {
                TextView text, date;
                TextView author;
                LinearLayout subscript;
            }

            private final LayoutInflater inflater;
            private final SimpleDateFormat df;

            public MyCursorAdapter(Context context, Cursor c, int flags) {
                super(context, c, flags);
                inflater = LayoutInflater.from(context);
                df = new SimpleDateFormat("d MMM");
            }

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                View view = inflater.inflate(R.layout.message_list_row, viewGroup, false);
                MyCursorAdapter.Holder h = new MyCursorAdapter.Holder();
                h.subscript = (LinearLayout) view.findViewById(R.id.subscript);
                h.text = (TextView) view.findViewById(android.R.id.text1);
                h.date = (TextView) view.findViewById(R.id.date);
                h.author = (TextView) view.findViewById(R.id.author);
                view.setTag(h);
                return view;
            }

            @Override
            // BaseColumns._ID, Helper.COLUMN_TEXT, Helper.COLUMN_DATE, Helper.COLUMN_AUTHOR, Helper.COLUMN_AUTHOR_TYPE
            public void bindView(View view, Context context, Cursor cursor) {
                MyCursorAdapter.Holder h = (MyCursorAdapter.Holder) view.getTag();
                h.text.setText(Html.fromHtml(cursor.getString(1)));
                h.date.setText(df.format(new java.util.Date(cursor.getLong(2))));

                String author = cursor.getString(3);
                if (TextUtils.isEmpty(author))
                    h.author.setText("");
                else
                    h.author.setText(author);

                int gravity = Gravity.RIGHT - cursor.getInt(4) * 2;

                h.subscript.setGravity(gravity);
                h.text.setGravity(gravity);
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            final Context context = getActivity();
            adapter = new MyCursorAdapter(context, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
            setListAdapter(adapter);
            getLoaderManager().initLoader(0, getArguments(), this);
            getLoaderManager().initLoader(1, getArguments(), new LoaderManager.LoaderCallbacks<AsyncResult<SessionParser.ParseThreadResult>>() {
                @Override
                public Loader<AsyncResult<SessionParser.ParseThreadResult>> onCreateLoader(int i, Bundle bundle) {
                    AccountManager accountManager = AccountManager.get(context);
                    Account[] accounts = accountManager.getAccountsByType(getString(R.string.account_type));
                    if (accounts.length <= 0) return null;

                    long id = bundle.getLong(BaseColumns._ID);
                    return new ThreadLoader(context, id, accounts[0]);
                }

                @Override
                public void onLoadFinished(Loader<AsyncResult<SessionParser.ParseThreadResult>> objectLoader, AsyncResult<SessionParser.ParseThreadResult> o) {
                    if (o.getException() != null) {
                        Log.e(TAG, "onThreadDowloadFinished", o.getException());
                        return;
                    }
                    DbManager.upsertMessages(getActivity().getApplicationContext(), o.getData());
                }

                @Override
                public void onLoaderReset(Loader<AsyncResult<SessionParser.ParseThreadResult>> objectLoader) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }
            }).forceLoad();

//            getListView().setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
//            getListView().setMultiChoiceModeListener(this);

            getListView().setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            getListView().setStackFromBottom(true);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
            switch (i) {
                case 0:
                    return DbManager.getThread(getActivity(), bundle.getLong(BaseColumns._ID));
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
            try {
                if (cursor.isClosed()) return;

                if (cursorLoader.getId() == 0) {
                    adapter.swapCursor(cursor);
                }
            } catch (Exception ignore) {
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> cursorLoader) {
            if (cursorLoader.getId() == 0) adapter.swapCursor(null);
        }
    }
}