package ua.veon.buyincoins.models;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/24/13
 * Time: 11:22 PM
 */

/*
Processing – Заказ обрабатывается (около 2 рабочих дней).

Processing-1 : Вы сделали заказ и магазин получил вашу оплату.
Processing-3: Магазин проверил ваш заказ, подтвердили что не имеется никаких проблем и готовы для печати почтовой метки.
Processing-5: Магазин напечатал почтовую метку и отправили заказ на склад, посылка готова к отправке.
Processing-2 и Processing-4: С вашим заказом проблемы, обратитесь в поддержку магазина.

Preparing for Ship –Подготовка к отправке. Заказы обычно отправляются в течении 1-2 рабочих дней.

Waiting for Dispatch - готовиться к отправке
Split Dispatching – Отправка по частям: некоторая часть ваших продуктов отправлена, остальные ждут отправки когда они появятся на складе.
Dispatched – Ваш заказ полностью отправлен.
Warning – Ваш заказ в ожидании, так как имеются проблемы с заказом/товаром.
Complete – Ваш заказ доставлен.
Refund – Возврат денег за заказ (или отдельные позиции).
Cancelled – Ваш заказ был отменен.
 */
public enum Status {
    UNKNOWN,
    PROCESS_0,
    PROCESS_1,
    PROCESS_2,
    PROCESS_3,
    PROCESS_4,
    PROCESS_5,
    PREP_FOR_SHIP,
    WAIT_FOR_DISPATCH,
    SPLIT_DISPATCHING,
    DISPATCHED,
    WARNING,
    COMPLETE,
    REFUND,
    CANCELLED,
}
