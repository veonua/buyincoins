package ua.veon.buyincoins.models;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/13/13
 * Time: 12:32 AM
 */
public class OrderDynamic {
    public Status status_code;
    public String status_text;
    private String[] trackingNumbers;
    public String orderWarning;

    public void setTrackingNumbers(StringBuilder trackingNumbers) {
        this.trackingNumbers = trackingNumbers.toString().trim().split(", ");
    }

    public String[] getTrackingNumbers() {
        return trackingNumbers;
    }
}
