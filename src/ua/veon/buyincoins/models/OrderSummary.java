package ua.veon.buyincoins.models;

import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/13/13
 * Time: 1:24 AM
 */
public class OrderSummary {
    public OrderDynamic status;

    public String deliveryAddress;
    public Date orderDate;
    public String paymentMethod;
    public String courierService;
    public Map<String, String> totals;
}
