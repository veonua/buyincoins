package ua.veon.buyincoins.models;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 2/3/13
 * Time: 1:48 AM
 */
public class MessageThreadRecord {

    public boolean is_new_for_cs;
    public final int id;
    public boolean is_new;
    public String subject;
    public Date create_time;
    public Date replied_time;
    public String last_replied_by;

    public MessageThreadRecord(int id) {
        this.id = id;
    }
}
