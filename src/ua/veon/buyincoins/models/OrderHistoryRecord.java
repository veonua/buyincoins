package ua.veon.buyincoins.models;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/21/13
 * Time: 12:29 AM
 */
public class OrderHistoryRecord {
    public String id;
    public String status;
    public Date date;
    public String recipient;
    public String link;
    public int productCount;
    public Double price;
}
