package ua.veon.buyincoins.models;

import android.text.TextUtils;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/24/13
 * Time: 1:28 AM
 */
public class OrderLocalModel implements Cloneable {
    public final String order_id;
    private String comment;
    private Date date;

    private boolean commentChanged, dateChanged;
    private Status status;
    private String status_text;
    private boolean statusChanged;

    public OrderLocalModel(final String id) {
        if (TextUtils.isEmpty(id)) throw new NullPointerException("id");
        order_id = id;
        status = Status.UNKNOWN;
        date = null;
    }

    public void flagSaved() {
        commentChanged = false;
        statusChanged = false;
        dateChanged = false;
    }

    public boolean isCommentChanged() {
        return commentChanged;
    }

    public boolean isDateChanged() {
        return dateChanged;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        this.commentChanged = true;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        this.dateChanged = true;
    }

    public boolean isStatusChanged() {
        return statusChanged;
    }

    public void setStatus(Status status, String status_text) {
        this.status = status;
        this.status_text = status_text;
        statusChanged = true;
    }

    public Status getStatus() {
        return status;
    }

    public String getStatusString() {
        return this.status_text;
    }
}
