package ua.veon.buyincoins.models;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 2/5/13
 * Time: 4:08 PM
 */
public class MessageRecord {
    public String text;
    public Date create_time;
    public String author;
    public int author_type;
}
