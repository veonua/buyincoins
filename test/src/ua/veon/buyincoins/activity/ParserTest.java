package ua.veon.buyincoins.activity;

import ua.veon.buyincoins.controllers.SessionParser;

import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: Veon
 * Date: 1/13/13
 * Time: 12:49 AM
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class ParserTest extends android.test.InstrumentationTestCase {

    public void testOrderParser() throws Exception {
        InputStream input = getInstrumentation().getContext().getAssets().open("logonOrder1.txt");

        //OrderParser.parse(input, false);
        input.close();
    }

    public void testHistoryParser() throws Exception {
        InputStream input = getInstrumentation().getContext().getAssets().open("order_history.txt");

        SessionParser sessionParser = new SessionParser();
        sessionParser.parseOrderHistory(input);

        input.close();
    }

    public void testMessagesParser() throws Exception {
        InputStream input = getInstrumentation().getContext().getAssets().open("message.txt");

        int size = input.available();
        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();

        SessionParser sessionParser = new SessionParser();
        sessionParser.parseMessagesHistory(new String(buffer));
    }

    public void testThreadParser() throws Exception {
        InputStream input = getInstrumentation().getContext().getAssets().open("read_thread.txt");

        int size = input.available();
        byte[] buffer = new byte[size];
        input.read(buffer);
        input.close();

        SessionParser sessionParser = new SessionParser();
        sessionParser.parseThread(new String(buffer));
    }

    public void testSessionParser() throws Exception {
        SessionParser sessionParser = new SessionParser();
        sessionParser.openSession("makeup@annasuperson.com", "010885");
    }
}
